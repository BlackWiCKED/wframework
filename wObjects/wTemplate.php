<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wTemplate {

    private $template = "";
    private $params = array();
    private $options = array();

    function __construct(&$parent, $file = "", $params = array()) {
        $this->parent = &$parent;
        if (!empty($file)) {
            $this->template = $this->load($file, true);
        }
        if (!empty($params)) {
            $this->params = $params;
        }
    }

    public function load($file, $return = false) {

        $templateContents = "";

        ob_start();

        if (file_exists($this->parent->frameworkDirectories["templates"] . $file . ".tpl")) {
            include ($this->parent->frameworkDirectories["templates"] . $file . ".tpl");
            $templateContents = ob_get_contents();
        } else {
            $templateContents = "";
        }

        ob_end_clean();

        if ($return) {
            return $templateContents;
        }
    }

    public function addOption($option) {
        $this->options[] = $option;
    }

    public function addParam($key, $value) {
        $this->addParams(array($key => $value));
    }

    public function addParams($params, $prefix = "") {
        foreach ($params as $key => $value) {
            $this->params[$prefix . $key] = $value;
        }
    }

    function render() {

        $search = array();
        $replace = array();

        foreach ($this->params as $key => $value) {
            $search[] = "$" . strtoupper($key) . "$";

            /*
              if (in_array("nl2br", $this->options)) {
              $value = nl2br($value);
              }
             */

            $replace[] = $value;
        }

        $template = str_replace($search, $replace, $this->template);

        return $template;
    }

}

?>
