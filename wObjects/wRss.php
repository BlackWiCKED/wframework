<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wRss {

    private $details;
    private $items;

    public function __construct($details, $items) {

        $this->details = $details;
        $this->items = $items;
    }

    public function createRss() {
        return $this->createFrame($this->getDetails() . $this->getItems());
    }

    private function createFrame($content) {

        $content = '<?xml version="1.0" encoding="UTF-8" ?>
				<rss version="2.0">
					<channel>' . $content . '</channel>
				</rss>';

        return $content;
    }

    private function getDetails() {

        $details = '';
        if (!empty($this->details)) {
            $details .= $this->parse($this->details);
        }

        return $details;
    }

    private function parse($data) {

        $xml = '';

        foreach ($data as $key => $value) {
            $details .= '<' . $key . '>';

            if (is_array($value)) {
                $details .= $this->parse($value);
            } else {

                if ($key == "description") {
                    $details .= '<![CDATA[' . $value . ']]>';
                } else {
                    $details .= $value;
                }
            }

            $details .= '</' . $key . '>' . "\n";
        }

        return $details;
    }

    private function getItems() {

        $items = '';
        if (!empty($this->items)) {
            foreach ($this->items as $item) {
                $items .= '<item>' . $this->parse($item) . '</item>';
            }
        }

        return $items;
    }

}

?>