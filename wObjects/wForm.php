<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wForm {

    private $header = array();
    private $data = array();
    private $errors = array();

    function __construct(&$parent, $header = array(), $data = array()) {

        $this->view = &$parent->view;
        $this->header = $header;
        $this->data = $data;

        $this->setDefaults();
    }

    private function setDefaults() {

        if (empty($this->header['params'])) {
            $this->header['params'] = array();
        }

        if (!isset($this->header['params']['class'])) {
            $this->header['params']['class'] = "default";
        }

        if (!isset($this->header['params']['prefix'])) {
            $this->header['params']['prefix'] = "";
        }

        if (empty($this->header['options'])) {
            $this->header['options'] = array();
        }

        if (empty($this->header['errors'])) {
            $this->header['errors'] = array();
        }

        if (empty($this->header['errors']["errorMandatory"])) {
            $this->header['errors']["errorMandatory"] = '%title% is mandatory!';
        }

        if (empty($this->header['errors']["errorNotValid"])) {
            $this->header['errors']["errorNotValid"] = '%title% is not valid!';
        }
    }

    function setHeader($header) {
        $this->header = $header;
    }

    function setData($data) {
        $this->data = $data;
    }

    function loadData($data) {
        foreach ($data as $key => $value) {
            if (isset($this->data[$key])) {
                $this->data[$key]['value'] = $value;
            }
        }
    }

    function clearData() {
        foreach ($this->data as $key => $value) {
            unset($this->data[$key]['value']);
        }
    }

    function checkData() {

        $this->errors = array();

        foreach ($this->data as $field => $data) {

            if (!empty($data['mandatory']) && empty($data['value'])) {
                $this->data[$field]['error'] = true;
                $this->errors[] = str_replace('%title%', $data['title'], $this->header['errors']["errorMandatory"]);
            }

            if ($data['type'] == "email" && !empty($data['mandatory'])) {
                $validEmail = filter_var($data['value'], FILTER_VALIDATE_EMAIL);
                if (empty($validEmail)) {
                    $this->data[$field]['error'] = true;
                    $this->errors[] = str_replace('%title%', $data['title'], $this->header['errors']["errorNotValid"]);
                }
            }
        }

        return empty($this->errors);
    }

    public function addError($error) {
        $this->errors[] = $error;
    }

    public function getValues($keys = array()) {

        $values = array();
        foreach ($this->data as $field => $data) {
            if (in_array($field, $keys) || empty($keys)) {
                if (!empty($data['value'])) {

                    if ($data['type'] == "timecycle") {
                        $valueArray = explode("||", $data['value']);
                        $values[$field] = $data['values'][$valueArray[0]] . " " . $valueArray[1] . "h-" . $valueArray[2] . "h";
                    } elseif ($data['type'] == "radio" || $data['type'] == "select") {
                        $values[$field] = $data['values'][$data['value']];
                    } else {
                        $values[$field] = $data['value'];
                    }
                }
            }
        }

        return $values;
    }

    function getErrors($string = false) {

        $ret = $this->errors;
        if ($string) {
            $ret = implode('<br/>', $this->errors);
        }
        return $ret;
    }

    function renderAll() {

        $headArray = array();
        foreach ($this->header['head'] as $key => $value) {
            $headArray[] = $key . '="' . $value . '"';
        }

        $formView = '<form ' . implode(" ", $headArray) . '>';

        if (!empty($this->header['options']['showLayout'])) {
            if ($this->header['options']['showLayout'] == "table") {
                $formView .= '<table class="' . $this->header['params']['class'] . 'Table" cellpadding="0" cellspacing="0">';
            }
        }

        if (empty($this->header['options']['hideErrors'])) {
            if (!empty($this->errors)) {
                $formView .= '<tr class="' . $this->header['params']['class'] . 'ErrorRow"><td class="' . $this->header['params']['class'] . 'ErrorCell" colspan="2"><div class="' . $this->header['params']['class'] . 'ErrorContainer">';
                $formView .= implode('<br/>', $this->errors);
                $formView .= '</div></td></tr>';
            }
        }

        $buttons = array();
        $hidden = array();

        foreach ($this->data as $field => $data) {

            $regular = !in_array($data['type'], array("submit", "button", "hidden"));

            $dataWithoutExtra = $data;

            $value = "";
            if (!empty($data['value'])) {
                $value = $data['value'];
                unset($dataWithoutExtra['value']);
            }

            $type = "";
            if (!empty($data['type'])) {
                $type = $data['type'];
                unset($dataWithoutExtra['type']);
            }

            $values = array();
            if (!empty($data['values'])) {
                $values = $data['values'];
                if (!is_array($values)) {
                    $values = explode("||", $values);
                }
                unset($dataWithoutExtra['values']);
            }

            $extra = array();
            if (!empty($dataWithoutExtra)) {
                $extra = $dataWithoutExtra;
            }

            if (!empty($this->header['options']['showLayout']) && $regular) {
                if ($this->header['options']['showLayout'] == "table") {
                    $formView .= '<tr class="' . $this->header['params']['class'] . 'TableRow ' . (!empty($data['error']) ? 'error' : '') . '">';
                    if (!empty($this->header['options']['showTitles'])) {
                        $formView .= '<td class="' . $this->header['params']['class'] . 'TableTitle">' . ($data['type'] != "submit" ? $data['title'] . ':' . (!empty($extra['mandatory']) ? ' *' : '') : "&nbsp;") . '</td>';
                    }
                    $formView .= '<td class="' . $this->header['params']['class'] . 'TableCell ' . $this->header['params']['class'] . ucfirst($field) . 'Holder">';
                }
            }

            if (!empty($this->header['options']['showWidgets']) && $regular) {
                $formView .= '<div class="' . $this->header['params']['class'] . 'Widget">';
            }

            if ($data['type'] == "button" || $data['type'] == "submit") {
                $buttons[] = $this->renderSingle($field, $data['title'], $type, $values, $extra, true);
            } else {
                $formView .= $this->renderSingle($field, $value, $type, $values, $extra, true);
            }

            if (!empty($data['info'])) {
                $formView .= '<br /><small><span class="' . $this->header['params']['class'] . 'TableInfo">(' . $data['info'] . ')</span></small>';
            }

            if (!empty($this->header['options']['showWidgets']) && $regular) {
                $formView .= '</div>';
            }

            if (!empty($this->header['options']['showLayout']) && $regular) {
                if ($this->header['options']['showLayout'] == "table") {
                    $formView .= '</td></tr>';
                }
            }
        }

        if (!empty($this->header['options']['showLayout'])) {
            if ($this->header['options']['showLayout'] == "table") {
                if (!empty($buttons)) {
                    $formView .= '<tr class="' . $this->header['params']['class'] . 'TableRow ' . (!empty($data['error']) ? 'error' : '') . '">';
                    $formView .= '<td></td><td class="' . $this->header['params']['class'] . 'TableCell ' . $this->header['params']['class'] . ucfirst($field) . 'Holder">';
                    $formView .= implode("", $buttons);
                    $formView .= '</td></tr>';
                }
                $formView .= '</table>';
            }
        }

        $formView .= '</form>';

        return $formView;
    }

    function renderSingle($field, $value, $type, $values, $extra = array(), $inner = false) {

        $elementView = "";

        $extraClass='';
        if (!empty($extra['class'])) {
            $extraClass .= ' '.trim($extra['class']);
        }
        $extraAttr = '';
        if (!empty($extra['attr'])) {
            foreach ($extra['attr'] as $k=>$v) {
                $extraAttr .= ' '.$k.'="'.$v.'"';
            }
        }

        if ($type == "date") {

            $startYear = date("Y") + 5;
            $endYear = date("Y") - 5;

            if (!empty($values)) {
                $startYear = $values['start'];
                $endYear = $values['end'];
            }

            $elementView = '<div id="datePicker' . ucfirst($field) . '" rel="' . $this->header['params']['prefix'] . ucfirst($field) . '">';

            $selectedYear = substr($value, 0, 4);
            $selectedMonth = intval(substr($value, 5, 2));
            $selectedDay = intval(substr($value, 8, 2));

            $elementView .= '<select class="datePickerYear">';
            $elementView .= '<option value="">----</option>';
            if ($startYear > $endYear) {
                for ($i = $startYear; $i > $endYear; $i--) {
                    $elementView .= '<option ' . ($i == $selectedYear ? 'selected="selected"' : '') . ' value="' . $i . '">' . $i . '</option>';
                }
            } else {
                for ($i = $startYear; $i < $endYear; $i++) {
                    $elementView .= '<option ' . ($i == $selectedYear ? 'selected="selected"' : '') . ' value="' . $i . '">' . $i . '</option>';
                }
            }
            $elementView .= '</select> ';

            $elementView .= '<select class="datePickerMonth">';
            $elementView .= '<option value="">--</option>';
            for ($i = 1; $i <= 12; $i++) {
                $elementView .= '<option ' . ($i == $selectedMonth ? 'selected="selected"' : '') . ' value="' . str_pad($i, 2, "0", STR_PAD_LEFT) . '">' . $i . '</option>';
            }
            $elementView .= '</select> ';

            $elementView .= '<select class="datePickerDay">';
            $elementView .= '<option value="">--</option>';
            for ($i = 1; $i <= 31; $i++) {
                $elementView .= '<option ' . ($i == $selectedDay ? 'selected="selected"' : '') . ' value="' . str_pad($i, 2, "0", STR_PAD_LEFT) . '">' . $i . '</option>';
            }
            $elementView .= '</select>';

            $elementView .= '</div>';

            $elementView .= '<input class="' . $this->header['params']['class'] . 'Element" type="hidden" id="' . $this->header['params']['prefix'] . ucfirst($field) . '" name="' . $field . '" value="' . $value . '" />';
        } elseif ($type == "timecycle") {

            $valueArray = explode("||", $value);

            if (!empty($valueArray[0])) {
                $rangePickerValue = $valueArray[0];
            } else {
                $rangePickerValue = '';
            }

            if (!empty($valueArray[1])) {
                $fromValue = $valueArray[1];
            } else {
                $fromValue = '';
            }

            if (!empty($valueArray[2])) {
                $toValue = $valueArray[2];
            } else {
                $toValue = '';
            }

            $elementView = '<div id="timeCycle' . ucfirst($field) . '" rel="' . $this->header['params']['prefix'] . ucfirst($field) . '">';

            $elementView .= '<select class="timecycleRangePicker">';
            foreach ($values as $valueKey => $valueItem) {
                $elementView .= '<option value="' . $valueKey . '" ' . ($valueKey == $rangePickerValue ? 'selected="selected"' : '') . '>' . $valueItem . '</option>';
            }
            $elementView .= '</select> ';

            $elementView .= '<select class="timecycleFromPicker">';
            for ($i = 6; $i <= 22; $i++) {
                $elementView .= '<option value="' . $i . '" ' . ($i == $fromValue ? 'selected="selected"' : '') . '>' . $i . 'h</option>';
            }
            $elementView .= '</select> - ';

            $elementView .= '<select class="timecycleToPicker">';
            for ($i = 6; $i <= 22; $i++) {
                $elementView .= '<option value="' . $i . '" ' . (($i == $toValue || empty($toValue) && $i == 22) ? 'selected="selected"' : '') . '>' . $i . 'h</option>';
            }
            $elementView .= '</select>';

            $elementView .= '</div>';

            $elementView .= '<input class="' . $this->header['params']['class'] . 'Element" type="hidden" id="' . $this->header['params']['prefix'] . ucfirst($field) . '" name="' . $field . '" value="' . $value . '" />';
        } elseif ($type == "textarea") {

            $elementView = '<textarea class="' . $this->header['params']['class'] . 'Element'.$extraClass.'" id="' . $this->header['params']['prefix'] . ucfirst($field) . '" name="' . $field . '">' . $value . '</textarea>';
        } elseif ($type == "bool") {

            $elementView = '<input type="hidden" name="' . $field . '" value="no" /><input ' . ($value == "yes" ? 'checked="checked"' : '') . ' class="' . $this->header['params']['class'] . 'Element ' . $this->header['params']['prefix'] . 'CheckboxStyle ' . $this->header['params']['prefix'] . ucfirst($field) . '" type="checkbox" name="' . $field . '" value="yes" />';
        } elseif ($type == "select") {

            $elementView = '<select class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" name="' . $field . '">';

            $associative = true;
            if (isset($extra['assoc']) && $extra['assoc'] == false) {
                $associative = false;
            }

            foreach ($values as $valueKey => $valueItem) {
                $elementView .= '<option value="' . ($associative ? $valueKey : $valueItem) . '" ' . (($associative ? $valueKey : $valueItem) == $value ? 'selected="selected"' : '') . '>' . $valueItem . '</option>';
            }

            $elementView .= '</select>';
        } elseif ($type == "radio") {

            $elementView = '<div id="radioHolder' . ucfirst($field) . '" rel="' . $this->header['params']['prefix'] . ucfirst($field) . '">';
            $count = 0;
            foreach ($values as $valueKey => $valueItem) {
                $elementView .= '<input type="radio" value="' . $valueKey . '" ' . ($valueKey == $value ? 'checked="checked"' : '') . ' class="' . $this->header['params']['class'] . 'Element ' . $this->header['params']['prefix'] . 'RadioStyle" id="' . $this->header['params']['prefix'] . ucfirst($field) . $count . '" name="' . $field . '" /> ' . $valueItem;
                if (!empty($extra['breakLine']))
                    $elementView .= '<br />';
                $count++;
            }
            $elementView .= "</div>";
        } elseif ($type == "tinymce") {

            $elementView = '<div style="height:280px; width:404px;"><input class="' . $this->header['params']['class'] . 'Element" type="hidden" id="' . $this->header['params']['prefix'] . ucfirst($field) . '" value="' . htmlspecialchars($value) . '" ' . ($inner ? 'name="' . $field . '"' : '') . ' /></div>';
        } elseif ($type == "image") {

            $elementView = '<div class="' . $this->header['params']['prefix'] . 'FileUploader">';
            $elementView .= '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['prefix'] . 'Element ' . (empty($value) ? 'none' : '') . '" type="input" name="' . $field . '" value="' . $value . '" />';
            $elementView .= '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . 'File ' . $this->header['params']['prefix'] . 'File" type="file" name="uploaded' . ucfirst($field) . '" />';

            $elementView .= '</div>';
        } elseif ($type == "button") {

            $elementView = '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" type="button" value="' . $value . '" />';
        } elseif ($type == "submit") {

            $elementView = '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" type="submit" value="' . $value . '" '.$extraAttr.' />';
        } elseif ($type == "hidden") {

            $elementView = '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" type="hidden" name="' . $field . '" value="' . $value . '" />';
        } elseif ($type == "password") {

            $elementView = '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" type="password" name="' . $field . '" value="' . $value . '" />';
        } elseif ($type == "text" || $type == "email") {

            $elementView = '<input class="' . $this->header['params']['prefix'] . ucfirst($field) . ' ' . $this->header['params']['class'] . 'Element'.$extraClass.'" type="text" name="' . $field . '" value="' . $value . '" />';
        } elseif ($type == "readonly") {

            $elementView = $value;
        }

        if (!$inner) {
            echo $elementView;
        }

        return $elementView;

    }

}
