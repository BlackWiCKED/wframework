<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wSeo {

    private $data = array(
        "language" => "en",
        "publisher" => "BlackWiCKED.com",
        "copyright" => "Creative Commons Attribution-Share Alike 3.0 Unported License",
        "author" => "BlackWiCKED.com",
        "distribution" => "global"
    );
    private $title = "";
    private $keywords = array(0 => array());
    private $description = "";
    private $properties = array();

    function __construct(&$parent = 0) {
        $this->parent = &$parent;
    }

    public function setMetaTag($key, $value) {
        $this->data[$key] = $value;
    }

    public function getMetaTag($key) {
        $ret = "";
        if (!empty($this->data[$key])) {
            $ret = $this->data[$key];
        }
        return $ret;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function addToTitle($title, $prepend = false, $separator = "-") {
        if ($prepend) {
            $this->title = $title . " " . $separator . " " . $this->title;
        } else {
            $this->title .= " " . $separator . " " . $title;
        }
    }

    public function getTitle() {
        return $this->title;
    }

    public function addKeywords($keywords, $priority = 0) {
        if (!is_array($keywords)) {
            $keywords = explode(",", $keywords);
        }
        foreach ($keywords as $keyword) {
            if (!isset($this->keywords[$priority])) {
                $this->keywords[$priority] = array(trim($keyword));
            } else {
                $this->keywords[$priority][] = trim($keyword);
            }
        }
    }

    public function getKeywords() {
        $keys = array();
        ksort($this->keywords);
        $sorted = array_reverse($this->keywords);
        foreach ($sorted as $priority) {
            foreach ($priority as $key) {
                $keys[] = $key;
            }
        }
        $keys = array_unique($keys);
        $ret = implode(", ", $keys);
        return $ret;
    }

    public function setDescription($desc) {
        $this->description = $desc;
    }

    public function getDescription() {
        return str_replace(array("\r\n", "\n", "\""), ' ', strip_tags($this->description));
    }

    public function addProperty($key, $value) {
        $this->properties[$key] = $value;
    }

    public function getProperties() {
        return $this->properties;
    }

}

?>
