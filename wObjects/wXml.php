<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wXml {

    public $format;
    public $header = array();
    public $data = array();
    public $options = array();
    public $result = "";
    private $formats = array('xml' => array(0 => "item"), 'rss' => array());

    function __construct($format, $header, $data, $options = array()) {

        if (in_array($format, array_keys($this->formats))) {
            $this->format = $format;
        }

        $this->header = $header;
        $this->data = $data;
        $this->options = $options;

        $this->init();
    }

    function init() {

        $this->result = "<" . $this->format . ">";

        if (!empty($this->header)) {
            $this->result .= "<header>";
            $this->createXml(1, $this->header, true);
            $this->result .= "</header>";
            $this->result .= "<data>";
            $this->createXml(1, $this->data);
            $this->result .= "</data>";
        } else {
            $this->createXml(0, $this->data);
        }

        $this->result .= "</" . $this->format . ">";
    }

    function createXml($szint, $data, $isHeader = false) {

        if (!empty($this->formats[$this->format][$szint - (!empty($this->header) ? 1 : 0)]) && !$isHeader) {
            $vanTag = $this->formats[$this->format][$szint - (!empty($this->header) ? 1 : 0)];
        }

        foreach ($data as $key => $item) {
            if (!empty($vanTag)) {
                $tag = $vanTag;
            } else {
                $tag = $key;
            }

            if (!(in_array("noItem", $this->options) && $tag == "item"))
                $this->result .= "<" . $tag . ">";

            if (is_array($item)) {
                $this->createXml($szint + 1, $item, $isHeader);
            } else {
                $this->result .= $item . "";
            }

            if (!(in_array("noItem", $this->options) && $tag == "item"))
                $this->result .= "</" . $tag . ">";
        }
    }

}

?>
