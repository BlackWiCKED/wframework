<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wMail {

    protected $html = "";
    protected $subject = "";
    protected $from = "root@localhost";
    protected $sender = "wMail";
    protected $to = array();
    protected $cc = array();
    protected $bcc = array();
    protected $usedEmails = array();

    function __construct(&$parent, $from = "", $sender = "") {

        if (!empty($from)) {
            $this->from = $from;
        }

        if (!empty($sender)) {
            $this->sender = $sender;
        }
    }

    public function html($html) {
        $this->html = $html;
    }

    public function subject($subject) {
        $this->subject = $subject;
    }

    public function from($from) {
        $this->from = $from;
    }

    public function sender($sender) {
        $this->sender = $sender;
    }

    public function send() {

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        if (!empty($this->to)) {
            $formatedTo = $this->createFormatedString($this->to);
        }

        if (!empty($this->cc)) {
            $formatedCc = $this->createFormatedString($this->cc);
            $headers .= 'Cc: ' . $formatedCc . "\r\n";
        }

        if (!empty($this->bcc)) {
            $formatedBcc = $this->createFormatedString($this->bcc);
            $headers .= 'Bcc: ' . $formatedBcc . "\r\n";
        }

        $headers .= 'From: ' . $this->sender . ' <' . $this->from . '>' . "\r\n";

        $headers .= 'Reply-To: ' . $this->sender . ' <' . $this->from . '>' . "\r\n";

        $headers .= 'X-Mailer: PHP/' . phpversion();

        $result = mail($formatedTo, $this->subject, $this->html, $headers);

        return $result;
    }

    public function addTo($to) {
        $this->prepareAdd("to", $to);
    }

    public function addCc($cc) {
        $this->prepareAdd("cc", $cc);
    }

    public function addBcc($bcc) {
        $this->prepareAdd("bcc", $bcc);
    }

    protected function prepareAdd($type, $emails) {

        if (is_array($emails)) {
            $emails = implode("; ", $emails);
        }
        $emailsArray = $this->parseFormatedString($emails);

        foreach ($emailsArray as $parsedEmails) {
            $this->add($type, $parsedEmails['email'], $parsedEmails['name']);
        }
    }

    protected function add($type, $email, $name) {

        if (!in_array($email, $this->usedEmails)) {
            $this->{$type}[] = array("name" => $name, "email" => $email);
            $this->usedEmails[] = $email;
        }
    }

    protected function parseFormatedString($source) {

        $result = array();

        $sourceArray = explode(",", $source);

        foreach ($sourceArray as $sourceItem) {

            preg_match('/(.*)?\<(.*)\>/', trim($sourceItem), $matches);

            if (empty($matches)) {
                $name = "";
                $email = trim($sourceItem);
            } else {
                $name = trim($matches[1]);
                $email = trim($matches[2]);
            }

            $result[] = array("name" => $name, "email" => $email);
        }

        return $result;
    }

    protected function createFormatedString($source) {

        $resultArray = array();
        foreach ($source as $emails) {
            if (!empty($emails['name'])) {
                $resultArray[] = $emails['name'] . ' <' . $emails['email'] . '>';
            } else {
                $resultArray[] = $emails['email'];
            }
        }

        $result = implode(", ", $resultArray);

        return $result;
    }

}

?>
