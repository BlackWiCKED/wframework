<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wLanguage {

    public $langFolder;
    private $currentLang;
    private $currentModule;
    private $availableLanguages;
    public $languageData = array();

    function __construct(&$parent, $cLang = "", $cModule = "") {

        $this->database = &$parent->database;

        $this->availableLanguages = $parent->languages;

        if (empty($cLang)) {
            $this->currentLang = $parent->session->wUser['lang'];
        } else {
            $this->currentLang = $cLang;
        }

        if (empty($cModule)) {
            $this->currentModule = $parent->module;
        } else {
            $this->currentModule = $cModule;
        }

        $this->langFolder = $parent->frameworkDirectories['lang'];

        foreach ($this->availableLanguages as $aLang) {
            $this->languageData[$aLang] = array();
        }
    }

    public function __toString() {
        return $this->currentLang;
    }

    function getLanguageFiles($type, $table = "", $option = array("global"), $cache = 0) {

        $lFiles = $option;
        $lFiles[] = $this->currentModule;

        if ($type == "file") {
            foreach ($lFiles as $lFile) {
                if (file_exists($this->langFolder . $lFile . ".lang")) {
                    $handle = @fopen($this->langFolder . $lFile . ".lang", "r");
                    if ($handle) {
                        while (!feof($handle)) {
                            $buffer = fgets($handle);
                            $data = explode("||", $buffer);
                            $keyWord = $data[0];
                            foreach ($data as $key => $value) {
                                $this->languageData[$this->availableLanguages[$key]][$keyWord] = trim($value);
                            }
                        }
                        fclose($handle);
                    }
                }
            }
        } elseif ($type == "database") {
            $result = array();
            $table = $this->database->prefix . $table;
            $languages = implode(",", $this->availableLanguages);
            $modules = "'" . implode("', '", $lFiles) . "'";

            $result = $this->database->queryArray("SELECT keyword," . $languages . " FROM " . $table . " WHERE module IN (" . $modules . ");", false, true, $cache);

            $this->languageDta = array();
            foreach ($result as $item) {
                foreach ($item as $key => $value) {
                    if (in_array($key, $this->availableLanguages)) {
                        $this->languageData[$key][$item['keyword']] = $value;
                    }
                }
            }
        }
    }

    function convert($keyWord, $lang = "") {
        if (empty($lang)) {
            $lang = $this->currentLang;
        }
        if (isset($this->languageData[$lang][$keyWord])) {
            return $this->languageData[$lang][$keyWord];
        }
        return $keyWord;
    }

    function search($searchTerm) {
        $langCondArray = array();
        foreach ($this->availableLanguages as $lang) {
            $langCondArray[] = '`' . $lang . '` LIKE "%' . $searchTerm . '%"';
        }
        $langCond = implode(" OR ", $langCondArray);
        $results = $this->database->queryArray("SELECT module, keyword, " . implode(",", $this->availableLanguages) . " FROM " . $this->database->prefix . "language WHERE " . $langCond . ";", false, true);
        $filtered = array();
        foreach ($results as $result) {
            $preview = array();
            foreach ($this->availableLanguages as $l) {
                $result[$l] = strip_tags($result[$l]);
                $start = strpos($result[$l], $searchTerm) - 40;
                if ($start < 0) {
                    $start = 0;
                }
                $preview[] = str_replace($searchTerm, '<span class="found">' . $searchTerm . '</span>', '.. ' . substr($result[$l], $start, strlen($searchTerm) + 80) . ' ..');
            }
            if (!isset($filtered[$result['module']])) {
                $filtered[$result['module']] = array();
            }
            $filtered[$result['module']][] = array('keyword' => $result['keyword'], 'preview' => $preview);
        }
        return $filtered;
    }

}

?>
