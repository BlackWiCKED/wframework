<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wFramework {

    public $frameworkDirectories = array();
    public $controllers = array();
    public $libraries = array();
    public $objects = array();
    public $registry;
    public $config;
    public $database;
    public $session;
    public $languages = array();
    public $prefix = "";
    public $baseUrl = "/";
    public $view;
    public $module = "index";
    public $application = "wInfo";
    private $args = array();
    private $showLayout = true;
    private $showView = true;
    
    private $exceptionHandler;
    private $defaultExceptionHandler = "wExceptionHandler::render";
    
    private $defaultControllers = array(
        "database" => "wDatabase",
        "layout" => "wLayout",
        "session" => "wSession",
        "cache" => "wCache",
        "console" => "wConsole",
        "cms" => "wCMS"
    );

    public function __construct(bool $autoInit = false) {

        define("wFrameWork", "<UNSTABLE VERSION>");

        $this->frameworkDirectories = array(
            "layout" => W_APP_PATH . "layout/",
            "scripts" => W_APP_PATH . "modules/",
            "lang" => W_APP_PATH . "lang/",
            "include" => W_APP_PATH . "include/",
            "pages" => W_APP_PATH . "html/",
            "templates" => W_APP_PATH . "templates/",
            "ownobjects" => W_APP_PATH . "objects/",
            "cachedir" => W_APP_PATH . "cache/",
            "api" => W_APP_PATH . 'api/',
            "images" => W_WEB_PATH . "gfx/",
            "style" => W_WEB_PATH . "css/",
            "flash" => W_WEB_PATH . "swf/",
            "javascript" => W_WEB_PATH . "js/",
            "controllers" => W_FRAME_PATH . "wControllers/",
            "objects" => W_FRAME_PATH . "wObjects/",
            "applications" => W_FRAME_PATH . "wApplications/"
        );

        $this->exceptionHandler = $this->defaultExceptionHandler;

        if ($autoInit) {
            $this->init(true);
        }
        
    }

    public function init($autoLoad = false) {

        try {

            session_start();

            $this->getControllers();
            $this->getObjects();

            $this->registry = new wRegistry();

            $configFile = W_APP_PATH . "config.ini";
            if (!defined('W_LIVE_VERSION') && file_exists(W_APP_PATH . "local.ini")) {
                $configFile = W_APP_PATH . "local.ini";
            }
            
            $this->config = new wConfig($configFile);
            $this->setDefaults();

            $this->router = new wRouter();
            $this->request("routes.php", false);

            $this->getArguments();

            $this->registry->config = $this->config->data;

            $this->databases = array();
            foreach (array_keys($this->config->data) as $db) {
                if (substr($db, 0, 8) == "database") {
                    $this->databases[] = $db;
                }
            }

            if (defined('W_DEBUG_VERSION')) {
                if (class_exists($this->defaultControllers['console'])) {
                    $this->console = new $this->defaultControllers['console']($this);
                }
            }

            foreach ($this->databases as $database) {
                if (class_exists($this->defaultControllers['database'])) {
                    $this->$database = new $this->defaultControllers['database']($this, $database);
                }
            }

            if (class_exists($this->defaultControllers['session'])) {
                $this->session = new $this->defaultControllers['session']($this, $this->module == "wFramework");
            }

            if (class_exists($this->defaultControllers['cache'])) {
                $this->cache = new $this->defaultControllers['cache']($this);
            }

            if (class_exists($this->defaultControllers['cms'])) {
                $this->cms = new $this->defaultControllers['cms']($this);
            }
            
        } catch (wException $e) {
            $this->handleException($e);
        }

        if ($autoLoad) {
            $this->load(true);
        }
        
    }

    public function load($autoShow = false) {

        try {
            if ($this->module == "api") {
                $this->view = new wLayout($this);
                if (file_exists($this->frameworkDirectories['applications'] . $this->application . ".php")) {
                    include_once ($this->frameworkDirectories['applications'] . $this->application . ".php");
                } elseif (file_exists($this->frameworkDirectories['api'] . $this->application . ".php")) {
                    include_once ($this->frameworkDirectories['api'] . $this->application . ".php");
                } else {
                    throw new wException('wApplication not found!', 404, $this);
                }
                $autoShow = false;
            } else {
                if (class_exists($this->defaultControllers['layout'])) {
                    $this->view = new $this->defaultControllers['layout']($this, $this->module);
                }
                if (file_exists($this->frameworkDirectories['scripts'] . $this->module . ".php")) {
                    include_once ($this->frameworkDirectories['scripts'] . $this->module . ".php");
                } else {
                    throw new wException('wModule not found!', 404, $this);
                }
            }
        } catch (wException $exception) {
            $this->handleException($exception);
        }

        if ($autoShow) {
            $this->show(true);
        }
        
    }

    public function show() {
        if (isset($this->view) && gettype($this->view) == "object" && get_class($this->view) == "wLayout") {
            if ($this->showView) {
                if ($this->showLayout) {
                    $this->view->render();
                } else {
                    $this->view->disableLayout();
                    $this->view->render();
                }
            }
        } else {
            if (file_exists($this->frameworkDirectories["pages"] . $this->module . ".html")) {
                include ($this->frameworkDirectories["pages"] . $this->module . ".html");
            }
        }
        $this->destroy();
    }

    private function destroy() {
        foreach ($this->databases as $database) {
            $this->$database->close();
        }
    }

    public function setDefaultController($controller, $value) {
        $this->defaultControllers[$controller] = $value;
    }

    private function setDefaults() {

        if (!empty($this->config->data['default'])) {
            if (!empty($this->config->data['default']['languages'])) {
                $this->languages = explode(",", $this->config->data['default']['languages']);
            } else {
                $this->languages = array("en");
            }

            if (!empty($this->config->data['default']['prefix'])) {
                $this->prefix = $this->config->data['default']['prefix'];
            } else {
                $this->prefix = "";
            }

            if (!empty($this->config->data['default']['baseUrl'])) {
                $this->baseUrl = $this->config->data['default']['baseUrl'];
            } else {
                $this->baseUrl = "/";
            }

            if (!empty($this->config->data['default']['domain'])) {
                $this->baseDomain = $this->config->data['default']['domain'];
            } else {
                $this->baseDomain = "";
            }

            if (!empty($this->config->data['default']['protocol'])) {
                $this->baseProtocol = $this->config->data['default']['protocol'];
            } else {
                $this->baseProtocol = "http://";
            }
        }
    }

    public function addLibrary($lib) {
        $this->libraries[] = $lib;
    }

    private function getControllers() {

        $controllerDir = opendir($this->frameworkDirectories['controllers']);
        while ($file = readdir($controllerDir)) {
            if ($file != '.' && $file != '..' && substr($file, 0, 1) != '.') {
                $this->controllers[] = $file;
                include_once ($this->frameworkDirectories['controllers'] . $file);
            }
        }
        closedir($controllerDir);
    }

    private function getObjects() {

        $objectDir = opendir($this->frameworkDirectories['objects']);
        while ($file = readdir($objectDir)) {
            if ($file != '.' && $file != '..' && substr($file, 0, 1) != '.') {
                $this->objects[] = $file;
                include_once ($this->frameworkDirectories['objects'] . $file);
            }
        }
        closedir($objectDir);

        if (file_exists($this->frameworkDirectories['ownobjects'])) {
            $objectDir = opendir($this->frameworkDirectories['ownobjects']);
            while ($file = readdir($objectDir)) {
                if ($file != '.' && $file != '..' && substr($file, 0, 1) != '.') {
                    if (!is_dir($this->frameworkDirectories['ownobjects'] . $file)) {
                        $this->objects[] = $file;
                        include_once ($this->frameworkDirectories['ownobjects'] . $file);
                    } else {
                        if (in_array($file, $this->libraries)) {
                            if (file_exists($this->frameworkDirectories['ownobjects'] . "/" . $file)) {
                                $libDir = opendir($this->frameworkDirectories['ownobjects'] . "/" . $file . "/");
                                while ($lib = readdir($libDir)) {
                                    if ($lib != '.' && $lib != '..' && substr($lib, 0, 1) != '.') {
                                        if (!is_dir($libDir . $lib)) {
                                            $this->objects[] = $lib;
                                            include_once ($this->frameworkDirectories['ownobjects'] . "/" . $file . "/" . $lib);
                                        }
                                    }
                                }
                                closedir($libDir);
                            }
                        }
                    }
                }
            }
            closedir($objectDir);
        }
    }

    private function getArguments($requestUri = "") {

        if (empty($requestUri)) {
            //IIS Fix
            if (!isset($_SERVER['REQUEST_URI'])) {
                $requestUri = "";
                if (!empty($_SERVER['HTTP_X_ORIGINAL_URL'])) {
                    $requestUri = substr($_SERVER['HTTP_X_ORIGINAL_URL'], 0);
                }
            } else {
                $requestUri = $_SERVER['REQUEST_URI'];
            }
        }

        $this->registry->location = $requestUri;

        $args = explode("/", substr($this->registry->location, strlen($this->baseUrl)));

        if (!isset($args[1])) {
            $args = explode("/", substr($this->registry->location, strlen($this->baseUrl)) . "/");
        }

        //GET SPECIAL VARS - Parsed first, if a specific key/value found defines the argument
        foreach ($this->router->specialVars as $spKey => $spValues) {
            foreach ($args as $key => $arg) {
                if ($arg == $spKey) {
                    if (empty($spValues) || in_array($args[$key + 1], $spValues)) {
                        $cutPart = array_splice($args, $key, 2);
                        $this->args[$cutPart[0]] = $cutPart[1];
                        break;
                    }
                }
            }
        }

        //GET GLOBAL VARS - After special vars uses the order to parse arguments with keys defined in routes.php
        foreach ($this->router->globalVars as $spKey => $spValues) {
            foreach ($args as $key => $arg) {
                if (empty($spValues) || in_array($args[$key], $spValues)) {
                    $cutPart = array_splice($args, $key, 1);
                    $this->args[$spKey] = $cutPart[0];
                    break;
                }
            }
        }

        if (!empty($args[0])) {
            $this->module = $args[0];
        }
        //GET APPLICATION - Module specific variable, only used with the API
        $start = 1;
        if ($this->module == "api") {
            if (!empty($args[1]) && (file_exists($this->frameworkDirectories['applications'] . $args[1] . ".php") || file_exists($this->frameworkDirectories['api'] . $args[1] . ".php") )) {
                $this->application = $args[1];
                $start = 2;
            }
        }

        //GET CUSTOM VARS - Module specific variables, only used with one module
        if (in_array($this->module, array_keys($this->router->routes))) {
            foreach ($this->router->routes[$this->module] as $customKey => $customVar) {
                if ($this->module != "api" || $customKey == $this->application) {
                    foreach ($customVar as $cv) {
                        if (!empty($args[$start])) {
                            $this->$cv = $args[$start];
                        } else {
                            $this->$cv = null;
                        }
                        $start++;
                    }
                }
            }
        }

        //GET STANDARD VARS - Parsing variables remained
        for ($i = $start; $i < count($args); $i+=2) {
            if (!empty($args[$i])) {
                if (!empty($args[$i + 1])) {
                    $this->args[$args[$i]] = $args[$i + 1];
                } else {
                    $this->args[$args[$i]] = null;
                }
            }
        }

        if (!empty($this->router->redirect[$this->module])) {
            $this->module = $this->router->redirect[$this->module];
        }
    }

    //GET URL ARGUMENTS
    public function arg($key, $default = null) {
        $return = $default;
        if (isset($this->args[$key])) {
            $return = $this->args[$key];
        }
        return $return;
    }

    //GET REQUEST VARIBLES
    public function req($key, $default = null) {
        $return = $default;
        if (isset($_REQUEST[$key])) {
            $return = $_REQUEST[$key];
        }
        return $return;
    }

    private function request($files, $mandatory = true) {
        if (!is_array($files)) {
            $files = array($files);
        }
        foreach ($files as $file) {
            if (file_exists($this->frameworkDirectories['include'] . $file)) {
                include ($this->frameworkDirectories['include'] . $file);
            } elseif ($mandatory) {
                throw new wException('Unable to include file!', 404, $this);
            }
        }
    }

    public function redirect($module = "index", $force = false) {
        if ($force) {
            $this->registry->location = $this->baseUrl . $module . "/";
        }
        $this->module = $module;
        $this->load(true);
        exit();
    }

    public function hideLayout() {
        $this->showLayout = false;
    }

    public function disableLayout() {
        $this->showView = false;
    }

    function setView($newView) {
        $this->view->setModule($newView);
    }

    function setLayout($newLayout) {
        $this->view->layout = $newLayout;
    }

    function setExceptionHandler($handleFunction) {
        $this->exceptionHandler = $handleFunction;
    }

    public function handleException($wException) {
        if ($wException->fatal) {
            if (defined('W_DEBUG_VERSION')) {
                call_user_func($this->defaultExceptionHandler, $wException);
            } else {
                echo "Fatal error.";
            }
            exit;
        } else {
            call_user_func($this->exceptionHandler, $wException);
        }
    }

}

?>
