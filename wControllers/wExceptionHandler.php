<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wExceptionHandler {

    public static function render($wException) {
        
        $string = '<table border="1"><tr><td colspan="2"><b>wFramework\'s wException [' . $wException->getCode() . ']</b><br/>' . $wException->getMessage() . '</td></tr>';
        $string .= '<tr><td><b>File:</b></td><td>' . $wException->getFile() . '</td></tr>';
        $string .= '<tr><td><b>Line:</b></td><td>' . $wException->getLine() . '</td></tr>';
        
        $string .= '<tr><td valign="top"><b>Trace:</b></td><td>';
        
        //echo '<pre>';
        foreach ($wException->getTrace() as $trace) {
            //var_dump($trace);
            $string .= $trace['class'] . " => " . $trace['function'] . '()<br/>'.$trace['file'].' ['.$trace['line'].']<br/>';
            $string .= '<hr/>';
        }
        //echo '/<pre>';
        
        $string .= '</td></tr></table>';

        echo $string;
        
    }
    
}

?>
