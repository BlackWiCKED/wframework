<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wException extends Exception {

    public $framework;
    public $fatal;

    public function __construct($message = null, $code = null, &$parent = null, $fatal = false) {
        $this->framework = $parent;
        $this->fatal = $fatal;
        parent::__construct($message, $code);
    }

}

?>
