<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wConsole {

    public $activity = array();

    function __construct(&$parent = 0) {
        $this->parent = &$parent;
    }

    function addActivity($activity) {
        $this->activity[] = $activity;
    }

    public function getmicrotime() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    public function debug($info) {
        $activity = array(
            'identifier' => 'console',
            'actor' => "debug",
            'information' => '<pre>' . var_export($info, true) . '</pre>',
            'success' => true
        );
        $this->activity[] = $activity;
    }

    public function info($info) {
        $activity = array(
            'identifier' => 'console',
            'actor' => "info",
            'information' => $info,
            'success' => true
        );
        $this->activity[] = $activity;
    }

    function renderActivities() {
        echo '<div style="display:block; padding-top:5px; margin-top:3px; border: 3px solid white; background-color: orange; color:black;"><span style="padding-left:5px; font-size:16px; font-weight:bold;">wConsole</span><br/><table cellpadding="0" cellspacing="0" border="0" style="width:100%; font-size:10px;">';
        $totalTime = 0;
        $totalTimeNotCached = 0;
        foreach ($this->activity as $act) {

            $bgcolor = "orange";
            $color = "black";
            if (!empty($act['cache'])) {
                $color = "green";
            }
            if (empty($act['success'])) {
                $color = "red";
            }
            if ($act['identifier'] == "console") {
                if ($act['actor'] == "debug") {
                    $color = "purple";
                } else {
                    $color = "blue";
                }
                $bgcolor = "white";
            } else {
                $timeTook = (ceil((double) (($act['stop'] - $act['start']) * 1000 * 1000)) / 1000);
                $totalTime += $timeTook;
                if (empty($act['cache'])) {
                    $totalTimeNotCached += $timeTook;
                }
            }
            echo '<tr style="background-color: ' . $bgcolor . '; color: ' . $color . ';">';
            echo '<td style="border-bottom: 1px dashed red; vertical-align:top; border-right: 1px dashed red; white-space: nowrap; padding-right: 5px; padding-left:5px;">' . $act['identifier'] . ': ' . $act['actor'] . '</td>';
            if ($act['identifier'] != "console") {
                echo '<td style="padding-left:5px; padding-right:5px; border-bottom: 1px dashed red;">' . htmlspecialchars($act['command'], ENT_QUOTES) . (!empty($act['cache']) ? ' [ CACHED ]' : '');
                if (!empty($act['information'])) {
                    echo '<br/><span style="color: red;">INFORMATION: ' . $act['information'] . '<span>';
                }
                echo '</td>';
                echo '<td style="vertical-align:top; border-bottom: 1px dashed red; border-left: 1px dashed red; white-space: nowrap; white-space: nowrap; padding-right:5px; padding-left:5px; text-align:right;">' . $timeTook . ' ms</td>';
            } else {
                echo '<td colspan="2" style="padding-left:5px; padding-right:5px; border-bottom: 1px dashed red; vertical-align:top;">' . (!empty($act['information']) ? $act['information'] : '') . '</td>';
            }
            echo '</tr>';
        }
        echo '</table><div style="padding:5px;"><b>Total time: </b>' . $totalTime . 'ms<br/><b>Not cached:</b> ' . $totalTimeNotCached . 'ms (' . number_format(100 * $totalTimeNotCached / $totalTime, 2) . '%)</div></div>';
    }

}

?>
