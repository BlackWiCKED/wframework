<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wConfig {

    public $data = array();
    private $file;

    function __construct($file) {
        $this->file = $file;
        $this->parseConfigFile();
    }

    function parseConfigFile() {
        $configFile = fopen($this->file, "r");
        $type = "";
        while (!feof($configFile)) {
            $line = trim(fgets($configFile));
            if (!empty($line) && substr($line, 0, 2) != "//") {
                if (substr($line, 0, 2) == "##") {
                    $type = trim(substr($line, 2));
                } else {
                    $option = explode("=", $line);
                    $this->data[$type][trim($option[0])] = trim($option[1]);
                }
            }
        }
        fclose($configFile);
    }

}

?>
