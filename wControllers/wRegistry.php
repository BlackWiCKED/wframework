<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wRegistry {

    private $data = array();

    public function __get($param) {

        if (isset($this->data[$param])) {
            return $this->data[$param];
        } elseif (isset($this->data['config']['default'][$param])) {
            return $this->data['config']['default'][$param];
        } else {
            return $this->data['default'][$param];
        }
    }

    public function __isset($param) {
        return isset($this->data[$param]);
    }

    public function __set($param, $value) {
        $this->data[$param] = $value;
    }

}

?>