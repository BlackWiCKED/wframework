<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wRouter {

    public $routes = array();
    public $redirect;
    public $specialVars = array();
    public $globalVars = array();

    function addRoute($module, $routeData = array(), $redirectDestination = null) {
        if (!empty($routeData)) {
            foreach ($routeData as $rk => $rd) {
                if (!is_array($rd)) {
                    $routeData[$rk] = array($rd);
                }
            }
            $this->routes[$module] = $routeData;
        }
        $this->redirect[$module] = $redirectDestination;
    }

    function addSpecialVar($key, $values = array()) {
        $this->specialVars[$key] = $values;
    }

    function addGlobalVar($key, $values = array()) {
        $this->globalVars[$key] = $values;
    }

}

?>
