<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wLayout {

    private $data = array();
    public $show;
    public $module;
    public $debug = false;
    public $layout = "layout";
    private $showLayout = true;

    public function __get($param) {

        if (isset($this->data[$param])) {
            return $this->data[$param];
        } else {
            return $this->data['default'][$param];
        }
    }

    public function __isset($param) {
        return isset($this->data[$param]);
    }

    public function __set($param, $value) {
        $this->data[$param] = $value;
    }

    function __construct(&$parent, $module = "index") {

        $this->parent = $parent;
        $this->module = $module;

        $this->baseUrl = $this->parent->registry->config['default']['baseUrl'];
        $this->languages = $this->parent->languages;
        if (isset($this->parent->cms)) {
            $this->cms = $this->parent->cms;
        }

        if (defined('W_DEBUG_VERSION')) {
            $this->debug = true;
            if (!empty($parent->console)) {
                $this->console = $parent->console;
            }
        }
    }

    public function disableLayout() {
        $this->showLayout = false;
    }

    public function setModule($module) {
        $this->module = $module;
    }

    function render($part = "", $cache = false) {

        if ($this->debug && !empty($this->console)) {
            $time_start = $this->console->getmicrotime();
        }

        $wasCached = false;

        ob_start();
        if (empty($part)) {
            if (empty($this->show)) {
                if (file_exists($this->parent->frameworkDirectories["pages"] . $this->module . ".html")) {
                    include ($this->parent->frameworkDirectories["pages"] . $this->module . ".html");
                } else {
                    throw new wException('Module not found!', 404, $this->parent);
                }
            } elseif (!is_array($this->show)) {
                if (file_exists($this->parent->frameworkDirectories["pages"] . $this->show)) {
                    include($this->parent->frameworkDirectories["pages"] . $this->show);
                } else {
                    throw new wException('Module not found!', 404, $this->parent);
                }
            } else {
                foreach ($this->show as $show) {
                    if (file_exists($this->parent->frameworkDirectories["pages"] . $show)) {
                        include($this->parent->frameworkDirectories["pages"] . $show);
                    } else {
                        throw new wException('Module not found!', 404, $this->parent);
                    }
                }
            }
            $this->content = ob_get_contents();
        } else {
            //if (empty($cache)) {
            if (file_exists($this->parent->frameworkDirectories["pages"] . $part)) {
                $cachedPartHtml = '';
                if (!empty($this->parent->cache) && !empty($cache)) {
                    $cachedPartHtml = $this->parent->cache->read("html", $part, $cache);
                }
                if (empty($cachedPartHtml)) {
                    include($this->parent->frameworkDirectories["pages"] . $part);
                    $partHtml = ob_get_contents();
                    if (!empty($this->parent->cache) && !empty($cache)) {
                        $this->parent->cache->write("html", $part, $partHtml);
                    }
                } else {
                    $partHtml = $cachedPartHtml;
                    $wasCached = true;
                }
            } else {
                //TODO: Normal message needed
                $partHtml = "Unable to render file: " . $part;
            }
            //}
        }
        ob_end_clean();

        if (empty($part)) {

            if ($this->showLayout) {
                if (file_exists($this->parent->frameworkDirectories["layout"] . $this->layout . ".html")) {
                    include_once ($this->parent->frameworkDirectories["layout"] . $this->layout . ".html");

                    if ($this->debug || !empty($this->console)) {
                        $time_end = $this->console->getmicrotime();
                        $this->console->addActivity(array(
                            'identifier' => 'layout',
                            'actor' => $this->layout . '.html',
                            'command' => 'LOAD LAYOUT ' . $this->layout . '.html',
                            'start' => $time_start,
                            'stop' => $time_end,
                            'success' => true
                        ));
                    }

                    if (defined('W_DEBUG_VERSION')) {
                        $this->console->renderActivities();
                    }
                }
            } else {
                echo $this->content;
            }
        } else {

            if ($this->debug || !empty($this->console)) {
                $time_end = $this->console->getmicrotime();
                $this->console->addActivity(array(
                    'identifier' => 'layout',
                    'actor' => $part,
                    'command' => 'LOAD VIEW ' . $part,
                    'cache' => $wasCached,
                    'start' => $time_start,
                    'stop' => $time_end,
                    'success' => true
                ));
            }

            return $partHtml;
        }
    }

}

?>
