<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wSession {

    public $wUser = array();
    public $wCookie = array();
    private $config = array();
    private $allowedUsers = array();
    private $langChange = "";

    function __construct(&$parent) {

        $this->database = &$parent->database;
        $this->sessionPrefix = &$parent->prefix;
        $this->cookiePrefix = &$parent->prefix;

        $this->languages = $parent->languages;

        $this->langChange = $parent->arg("lang");

        $this->writeBasicInfo();
        $this->refresh();
    }

    function refresh() {
        $this->wCookie = $_COOKIE;
        $this->wUser = $_SESSION[$this->cookiePrefix . '_wUser'];
    }

    function wUserData($key, $default = "") {
        $value = $default;
        if (!empty($this->wUser[$key])) {
            $value = $this->wUser[$key];
            //} elseif (!empty($this->wUser['userdata'][$key])) {
            //TODO: This is a bad solution
            //	$value = $this->wUser['userdata'][$key];
        }
        return $value;
    }

    function getCookie($name) {
        $this->refresh();
        $ret = null;
        if (!empty($this->wCookie[$this->cookiePrefix . "_" . $name])) {
            $ret = $this->wCookie[$this->cookiePrefix . "_" . $name];
        }
        return $ret;
    }

    function addCookie($name, $value, $days = 365, $hours = 0, $path = "/", $domain = "") {
        //var_dump($days);
        //var_dump($hours);
        setcookie($this->cookiePrefix . "_" . $name, $value, time() + $days * 24 * 60 * 60 * 1000 + $hours * 60 * 60, $path, $domain);
    }

    function deleteCookie($name) {
        setcookie($this->cookiePrefix . "_" . $name, "", time() - 3600, "/");
    }

    function writeBasicInfo() {

        if (empty($_SESSION[$this->cookiePrefix . '_wUser'])) {

            //if (!empty($_COOKIE[$this->cookiePrefix."_".'lang'])) {
            //	$langAct = $_COOKIE[$this->cookiePrefix."_".'lang'];
            //} else {
            $langAct = $this->languages[0];
            //$this->addCookie("lang", $this->languages[0]);
            //}

            if (!empty($_COOKIE[$this->cookiePrefix . "_" . 'unique'])) {
                $uniqueId = $_COOKIE[$this->cookiePrefix . "_" . 'unique'];
            } else {
                $uniqueId = md5(uniqid(mt_rand(), true));
                //$this->addCookie("unique", $uniqueId);
            }

            $_SESSION[$this->cookiePrefix . '_wUser'] = array(
                'lang' => $langAct,
                'unique' => $uniqueId/* ,
                      'userdata' => array()
                     */
            );
        }

        //$uniqueCookie = $this->getCookie("unique");
        //if (empty($uniqueCookie)) {
        //$this->addCookie("unique",$this->wUserData("unique"));
        //}

        $_SESSION[$this->cookiePrefix . '_wUser']['ip'] = $_SERVER['REMOTE_ADDR'];

        if (!empty($this->langChange) && $this->langChange != $_SESSION[$this->cookiePrefix . '_wUser']['lang'] && in_array($this->langChange, $this->languages)) {
            $_SESSION[$this->cookiePrefix . '_wUser']['lang'] = $this->langChange;
            $this->addCookie("lang", $this->langChange);
        }
    }

}

?>
