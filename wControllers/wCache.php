<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wCache {

    private $cacheRoot = "";
    private $cacheLang = "";
    private $debug = false;

    function __construct(&$parent = 0) {
        $this->parent = $parent;
        $this->cacheRoot = $this->parent->frameworkDirectories['cachedir'];
        if (defined('W_DEBUG_VERSION')) {
            $this->debug = true;
            if (!empty($parent->console)) {
                $this->console = $parent->console;
            }
        }
    }

    function read($type, $id, $time = 0) {

        if ($this->debug) {
            $time_start = $this->console->getmicrotime();
        }
        
        $content = false;
        $file = $this->getFileLocation($type, $id);
        if (file_exists($file) && ($time == 0 || (time() - $time < filemtime($file)))) {
            $content = file_get_contents($file);
            if ($type == "db" || $type == "php") {
                $content = unserialize($content);
                if ($this->debug || !empty($this->console)) {
                    $time_end = $this->console->getmicrotime();
                    $this->console->addActivity(array('identifier' => 'cache', 'actor' => $type, 'command' => $id, 'start' => $time_start, 'stop' => $time_end, 'success' => true, 'information' => ''/*$this->queryInformation['errorString']*/, "cache" => true));
                }
            }
        }

        return $content;
    }

    function write($type, $id, $content) {

        $fp = fopen($this->getFileLocation($type, $id), "w");
        if ($type == "html") {
            fputs($fp, "<!-- wFramework - CACHED: " . $id . " - " . date("Y-m-d H:i:s") . " -->\n" . $content);
        } elseif ($type == "db" || $type == "php") {
            fputs($fp, serialize($content));
        }
        fclose($fp);
    }

    private function getFileLocation($type, $id) {
        return $this->cacheRoot . $type . ((($type == "html" || $type == "php") && !empty($this->parent->view->lang)) ? '/' . $this->parent->view->lang : '') . '/' . md5($id);
    }

}

?>
