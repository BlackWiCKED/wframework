<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

class wDatabase {

    public $connection;
    public $debug = false;
    public $prefix = "";
    private $dbData = array();
    private $identifier;
    private $queryInformation = array();

    function __construct(&$parent = null, $database = "database") {

        $this->parent = $parent;
        $this->identifier = $database;

        if (is_array($database)) {
            $this->init($database);
        } else {
            if (!empty($this->parent)) {
                $this->init($parent->registry->config[$this->identifier]);
            }
        }

        if (defined('W_DEBUG_VERSION')) {
            $this->debug = true;
            if (!empty($parent->console)) {
                $this->console = $parent->console;
            }
        }
    }

    function init($dbData) {
        $this->dbData = $dbData;
        if (!empty($dbData['prefix'])) {
            $this->prefix = $dbData['prefix'] . "_";
        }
    }

    function connect() {
        if (empty($this->connection)) {
            $this->connection = @mysqli_connect($this->dbData['host'], $this->dbData['user'], $this->dbData['pass']);
            if (mysqli_connect_errno()) {
                throw new wException('wDatabase error: ' . mysqli_connect_error(), 500, $this->parent, true);
            } else {
                if (!$this->connection->select_db($this->dbData['database'])) {
                    throw new wException('wDatabase error: ' . $this->connection->error, 500, $this->parent, true);
                } else {
                    $this->connection->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
                }
            }
        }
    }

    function close() {
        if (!empty($this->connection)) {
            $this->connection->close();
        }
        unset($this->connection);
    }

    function queryArray($query, $assoc = false, $multi = true, $cache = false) {

        $newarray = array();

        $cachedReturn = array();
        $cacheKey = $query . " " . (!$assoc ? '!' : '') . "ASSOC; " . (!$multi ? '!' : '') . "MULTI";
        if (!empty($this->parent->cache) && !empty($cache)) {
            $cachedReturn = $this->parent->cache->read("db", $cacheKey, $cache);
        }

        if (empty($cachedReturn)) {
            $return = array();
            $keres = $this->makeQuery($query);
            if (!$keres) {
                throw new wException('wDatabase error: ' . $query, 500, $this->parent);
            } else {
                if ($assoc) {
                    while ($row = $keres->fetch_array()) {
                        foreach ($row as $key => $value) {
                            if (!is_int($key)) {
                                $newarray[$row[0]][$key] = $value;
                            }
                        }
                    }
                } else {
                    while ($row = $keres->fetch_assoc()) {
                        array_push($newarray, $row);
                    }
                }

                if ($multi) {
                    $return = $newarray;
                } else {
                    if ($assoc) {
                        if (!empty($newarray[0])) {
                            $return = $newarray[$row[0]];
                        }
                    } else {
                        if (!empty($newarray[0])) {
                            $return = $newarray[0];
                        }
                    }
                }
            }

            if (!empty($this->parent->cache) && !empty($cache)) {
                $this->parent->cache->write("db", $cacheKey, $return);
            }
        } else {
            $return = $cachedReturn;
        }

        return $return;
    }

    function queryRow($query, $assoc = false) {
        return $this->queryArray($query, $assoc, false);
    }

    function queryGroup($query, $group, $assoc = false, $remain = false, $cache = false) {

        $cachedReturn = array();
        $cacheKey = $query . " " . (!$assoc ? '!' : '') . "ASSOC; " . (!$remain ? '!' : '') . "REMAIN; GROUP: " . $group;
        if (!empty($this->parent->cache) && !empty($cache)) {
            $cachedReturn = $this->parent->cache->read("db", $cacheKey, $cache);
        }

        if (empty($cachedReturn)) {
            $newarray = array();
            $temp = $this->queryArray($query);
            foreach ($temp as $key => $row) {
                $currentGroup = $row[$group];
                if (!$remain) {
                    unset($row[$group]);
                }
                if ($assoc) {
                    $assocKeyArray = array_keys($row);
                    $newarray[$currentGroup][$row[$assocKeyArray[0]]] = $row;
                } else {
                    $newarray[$currentGroup][] = $row;
                }
            }

            if (!empty($this->parent->cache) && !empty($cache)) {
                $this->parent->cache->write("db", $cacheKey, $newarray);
            }
        } else {
            $newarray = $cachedReturn;
        }

        return $newarray;
    }

    function queryList($query, $keys, $assoc = false, $cache = false) {

        if (!is_array($keys)) {
            $keys = array($keys);
        }

        $cachedReturn = array();
        $cacheKey = $query . " " . (!$assoc ? '!' : '') . "ASSOC";
        if (!empty($this->parent->cache) && !empty($cache)) {
            $cachedReturn = $this->parent->cache->read("db", $cacheKey, $cache);
        }

        if (empty($cachedReturn)) {

            $newarray = array();

            $keres = $this->makeQuery($query);

            if ($assoc) {
                while ($row = $keres->fetch_array()) {
                    if (count($keys) == 1) {
                        $newarray[$row[0]] = $row[$keys[0]];
                    } else {
                        $newarray[$row[0]] = array();
                        foreach ($keys as $key) {
                            $newarray[$row[0]][$key] = $row[$key];
                        }
                    }
                }
            } else {
                while ($row = $keres->fetch_assoc()) {
                    if (count($keys) == 1) {
                        array_push($newarray, $row[$keys[0]]);
                    } else {
                        $elements = array();
                        foreach ($keys as $key) {
                            $elements[$key] = $row[$key];
                        }
                        array_push($newarray, $elements);
                    }
                }
            }

            if (!empty($this->parent->cache) && !empty($cache)) {
                $this->parent->cache->write("db", $cacheKey, $newarray);
            }
        } else {
            $newarray = $cachedReturn;
        }

        return $newarray;
    }

    function queryString($query, $key, $default = "") {
        $result = $default;
        $keres = $this->makeQuery($query);
        $row = $keres->fetch_assoc();
        if (isset($row[$key])) {
            $result = $row[$key];
        }
        return $result;
    }

    function query($query) {
        $keres = $this->makeQuery($query);

        return $keres;
    }

    function replace($table, $data) {
        $keres = $this->insert($table, $data, true);
        return $keres;
    }

    function update($table, $data, $where) {
        $table = "`" . $this->prefix . $table . "`";

        $dataArray = array();
        foreach ($data as $key => $value) {
            if (is_null($value)) {
                $dataArray[] = "`" . $key . "` = NULL";
            } else {
                $dataArray[] = "`" . $key . "` = '" . $this->escape($value) . "'";
            }
        }

        $query = "UPDATE " . $table . " SET " . implode(", ", $dataArray) . " WHERE " . $where . ";";
        $keres = $this->makeQuery($query);
        return $keres;
    }

    function insert($table, $data, $replace = false, $ignore = false) {
        $command = "INSERT " . ($ignore ? "IGNORE " : "") . "INTO ";
        if ($replace) {
            $command = "REPLACE INTO ";
        }
        $table = "`" . $this->prefix . $table . "`";
        $keys = "`" . implode("`, `", array_keys($data)) . "`";

        $escapedData = array();
        foreach ($data as $key => $item) {
            if (is_null($item)) {
                $escapedData[$key] = "NULL";
            } else {
                $escapedData[$key] = "'" . $this->escape($item) . "'";
            }
        }

        $query = $command . $table . " (" . $keys . ") VALUES (" . implode(",", $escapedData) . ");";
        $keres = $this->makeQuery($query);
        return $keres;
    }

    function delete($table, $where) {
        $table = "`" . $this->prefix . $table . "`";
        $query = "DELETE FROM " . $table . " WHERE " . $where . ";";
        $keres = $this->makeQuery($query);
        return $keres;
    }

    function getTables($cache = false) {

        $cachedReturn = array();
        $cacheKey = 'LIST TABLES';
        if (!empty($this->parent->cache) && !empty($cache)) {
            $cachedReturn = $this->parent->cache->read("db", $cacheKey, $cache);
        }

        if (empty($cachedReturn)) {

            $tables = array();
            if ($this->debug || !empty($this->console))
                $time_start = $this->console->getmicrotime();

            $this->connect();
            $tableList = $this->connection->query("SHOW TABLES FROM " . $this->dbData['database']);

            if ($this->debug || !empty($this->console)) {
                $time_end = $this->console->getmicrotime();
                $this->console->addActivity(array('identifier' => $this->identifier, 'actor' => $this->dbData['database'], 'command' => "LIST TABLES", 'start' => $time_start, 'stop' => $time_end, 'success' => true));
            }
            while ($table = $tableList->fetch_array()) {
                array_push($tables, $table[0]);
            }

            if (!empty($this->parent->cache) && !empty($cache)) {
                $this->parent->cache->write("db", $cacheKey, $tables);
            }
        } else {
            $tables = $cachedReturn;
        }

        return $tables;
    }

    function getFields($tableName, $cache = false) {

        $query = "SHOW COLUMNS FROM `" . $tableName . "`;";

        $cachedReturn = array();
        $cacheKey = $query;
        if (!empty($this->parent->cache) && !empty($cache)) {
            $cachedReturn = $this->parent->cache->read("db", $cacheKey, $cache);
        }

        if (empty($cachedReturn)) {

            $newarray = array();
            $keres = $this->makeQuery($query);
            while ($row = $keres->fetch_assoc()) {
                array_push($newarray, $row);
            }


            if (!empty($this->parent->cache) && !empty($cache)) {
                $this->parent->cache->write("db", $cacheKey, $newarray);
            }
        } else {
            $newarray = $cachedReturn;
        }

        return $newarray;
    }

    function getIndexes($tableName) {

        $newarray = array();
        $query = "SHOW KEYS FROM `" . $tableName . "`;";
        $keres = $this->makeQuery($query);
        while ($row = $keres->fetch_assoc()) {

            if ($row['Key_name'] == "PRIMARY") {
                $newarray[$row['Key_name']][$row['Column_name']] = 'PRI';
            } else {
                $newarray[$row['Key_name']][$row['Column_name']] = (empty($row['Non_unique'])) ? 'UNI' : 'MUL';
            }
        }
        return $newarray;
    }

    function countResults($query) {
        $keres = $this->makeQuery($query);
        $szam = intval($keres->num_rows);
        return $szam;
    }

    function lastInsertId() {
        return $this->queryInformation['lastInsertId'];
    }

    function affectedRows() {
        return $this->queryInformation['affectedRows'];
    }

    function errorString() {
        return $this->queryInformation['errorString'];
    }

    function foundRows() {
        $rowCount = 0;
        if (!empty($this->queryInformation['foundRows'])) {
            $rowCount = $this->queryInformation['foundRows'];
        }
        return $rowCount;
    }

    private function makeQuery($query) {

        if ($this->debug || !empty($this->console)) {
            $time_start = $this->console->getmicrotime();
        }

        $this->connect();

        $keres = $this->connection->query($query);

        $this->queryInformation['lastInsertId'] = $this->connection->insert_id;
        $this->queryInformation['affectedRows'] = $this->connection->affected_rows;
        $this->queryInformation['errorString'] = $this->connection->error;

        if (strpos($query, "SQL_CALC_FOUND_ROWS")) {
            $cr = $this->connection->query("SELECT FOUND_ROWS() AS cr;");
            $row = $cr->fetch_assoc();
            $this->queryInformation['foundRows'] = $row['cr'];
        }

        if ($this->debug || !empty($this->console)) {
            $result = empty($keres) ? false : true;
            $time_end = $this->console->getmicrotime();
            $this->console->addActivity(array('identifier' => $this->identifier, 'actor' => $this->dbData['database'], 'command' => $query, 'start' => $time_start, 'stop' => $time_end, 'success' => $result, 'information' => $this->queryInformation['errorString']));
        }
        return $keres;
    }

    public function escape($string) {
        $this->connect();
        $string = $this->connection->real_escape_string($string);
        return $string;
    }

}

?>
