<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

$data = array(
    $this->registry->config['framework'],
    $this->registry->config['default'],
    $this->registry->config['page'],
    array('session' => session_id())
);

echo "<u><b>wFramework " . wFrameWork . "</b></u><br/>";

foreach ($data as $child) {
    if (is_array($child)) {
        foreach ($child as $key => $grandchild) {
            echo" <b>" . $key . ":</b> " . $grandchild . "<br/>";
        }
    }
}

?>
