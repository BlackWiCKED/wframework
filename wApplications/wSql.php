<?php

/**
 * This file is part of wFramework <UNSTABLE VERSION>
 * Author: Edvin Rab, BlackWiCKED.com
 * Location: Subotica, Serbia
 * Date: <UNSTABLE DATE>
 * Project: http://www.blackwicked.com/en/framework/
 * Documentation: http://www.blackwicked.com/en/framework/documentation/
 * Demonstration: http://demo.blackwicked.com/
 * License: Creative Commons Attribution-Share Alike 3.0 Unported License
 * License information: http://creativecommons.org/licenses/by-sa/3.0/
 */

header("content-type: text/xml");

$header = array("session" => session_id(), /* 'post'=>$_POST, */ 'logged_user' => $this->session->wSql['name']);
$data = array();

if (!empty($this->args['user'])) {
    $header['action'] = "user/" . $this->args['user'];
    switch ($this->args['user']) {
        case "login":
            if (!empty($_POST['user']) && !empty($_POST['pass'])) {
                if ($this->session->wSqlLogin($_POST['user'], $_POST['pass'])) {
                    $data = array(array('login' => 'ok'));
                }
            } else {
                $data = array(array('login' => 'error'));
            }
            break;
        case "logout":
            $this->session->wSqlLogout();
            $data = array(array('logout' => 'ok'));
            break;
        case "status":
            $data = array(array('status' => $this->session->wSql));
            break;
    }
    $xml = new xmlObject("xml", $header, $data, array('noItem'));
}

if (!empty($this->args['show'])) {
    $header['action'] = "show/" . $this->args['show'];
    switch ($this->args['show']) {
        case "tables": $data = $this->database->getTables();
            break;
        case "refreshfields":
        case "fields": $data = $this->database->getFields($this->args['params']);

            $header['indexes'] = $this->database->getIndexes($this->args['params']);

            break;
        case "refreshtable":
        case "table":

            $header['post'] = $_POST;

            if (!empty($header['post']['limit'])) {
                $limit = $header['post']['limit'] + 0;
            } else {
                $limit = 30;
            }

            if (!empty($header['post']['page'])) {
                $page = ($header['post']['page'] + 0) * $limit;
            } else {
                $page = 0;
            }

            $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $this->args['params'] . " LIMIT " . $page . ", " . $limit . ";";

            $header['sql'] = str_replace("SQL_CALC_FOUND_ROWS ", "", $sql);

            $data = $this->database->queryArray($sql);

            $header['start'] = $page;
            $header['totalRows'] = $this->database->queryString("SELECT FOUND_ROWS() AS countall", "countall") + 0;
            $header['totalPages'] = ceil($header['totalRows'] / $limit);

            break;
    }
    $xml = new xmlObject("xml", $header, $data);
}

if (!empty($this->args['query'])) {
    $header['action'] = "query/" . $this->args['query'];
    switch ($this->args['query']) {
        case "simple":

            //$header['file'] = file_get_contents("/home/wicked/file.txt");

            $postCommand = stripslashes(utf8_decode($_POST['queryString']));

            $getCommand = explode(" ", $postCommand);
            $command = $getCommand[0];

            $header['sql'] = $postCommand;

            if ($this->database->query($postCommand)) {

                $header['result'] = "success";

                if (in_array($command, array("INSERT", "UPDATE", "REPLACE", "DELETE"))) {
                    $header['affected'] = $this->database->affectedRows();
                    if ($command == "INSERT") {
                        $header['lastid'] = $this->database->lastInsertId();
                    }
                }
            } else {
                $header['result'] = "fail";
                $header['error'] = $this->database->errorString();
            }

            break;
    }
    $xml = new xmlObject("xml", $header, array());
}
?><?= $xml->result; ?>